# Task description
Create a package.json file
1. Install TypeScript as a dev dependency
2. Install rxjs library as a main dependency
3. Install json-server as a global package
4. Create a db.json file and add the following content to it:
```
{ 
  "tasks": [ 
    { 
      "id": 1, 
      "action": "Estimate", 
      "priority": 3, 
      "estHours": 8 
    }, 
    { 
      "id": 2, 
      "action": "Create", 
      "priority": 2, 
      "estHours": 8 
    }
  ] 
}
```
6. add the following command to the scripts section of the package json `` "start": "json-server --watch db.json" ``,

## Provide the mentor with:
* a screenshot of the content of the package.json file
* a screenshot of the content of a terminal window after
* executing the command "npm start"
* a link to the gitlab project with installed dependencies and dev-dependencies